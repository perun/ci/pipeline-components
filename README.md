# ci-tools

Prebuilt tools needed for pipelines.

## commitlint

A simple image for running Commitlint.

Example usage:

```yaml
commitlint:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/commitlint:latest
  script:
    - /app/commitlint.sh
  rules:
    - if: "$CI_MERGE_REQUEST_EVENT_TYPE != 'merge_train' && $CI_MERGE_REQUEST_DIFF_BASE_SHA"
```

## expo

Node.js image with eas-cli installed globally.

Example usage:

```yaml
commitlint:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/commitlint:latest
  script:
    - eas build
```

## minideb

Minimalistic image with ssh client, git, curl, jq, for running simple commands.

Example usage:

```yaml
commitlint:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/minideb:latest
  script:
    - git clone $CI_REPOSITORY_URL
```

## python-dev

Python image prepared for building wheels etc. during pip install.

Example usage:

```yaml
pip:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/python-dev:latest
  script:
    - |
      export PYTHONPATH=$PYTHONPATH:$PWD:/tmp/.local/lib/python3.11/site-packages
    - virtualenv venv
    - source venv/bin/activate
    - pip3 install -t /tmp/.local/lib/python3.11/site-packages -r requirements.txt
    - python3 -m pylint
  variables:
    PIP_CACHE_DIR: "/tmp/.cache/pip"
```

## semantic-release

A simple image for running Semantic Release
with [@perun/semantic-release-perun-config](https://gitlab.ics.muni.cz/perun/semantic-release-perun-config).

Semantic release and its plugins are supposed to be installed in the current repository and run via `npx`.
It cannot be easily installed globally, so special care is needed.

In order to use this tool, one of the following must hold:

1. The repository is not an npm package, does not contain a `package.json` or a `.npmrc` file.
   In this case, everything is installed by this image.
2. The repository is an npm package, it contains a `package.json` file.
   In this case, semantic release and all its plugins have to be installed in the package (as `devDependencies`),
   so there needs to be a `.npmrc` file, at least for the `@perun/semantic-release-perun-config` package.

The script provided by this image will check for one of these cases.

Example usage:

```yaml
semantic-release:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/semantic-release:latest
  script:
    - /app/semantic-release.sh
  variables:
    NPM_AUTH_TOKEN: $NPM_AUTH_TOKEN # refer to this in the .npmrc file
    NPM_TOKEN: $CI_JOB_TOKEN # only required for publishing into the GitLab Package registry
```

The `NPM_AUTH_TOKEN` needs privileges to read the Perun proxy AAI GitLab Package registry.
The `NPM_TOKEN` is used by [@semantic-release/npm](https://github.com/semantic-release/npm/) during publishing.
It will be written to a temporary `.npmrc` file. You should also set `publishConfig` in `package.json` correctly, for example:

```json
  // ...
  "publishConfig": {
    "registry": "https://gitlab.ics.muni.cz/api/v4/projects/4411/packages/npm/",
  },
  // ...
```

where `4411` is the GitLab project ID.
