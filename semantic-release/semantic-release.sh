# check for invalid cases first
if [ -f package.json ] && [ ! -f .npmrc ]; then
    echo "package.json is present but .npmrc is missing, the release will not work";
    exit 1;
fi;
if [ ! -f package.json ] && [ -f .npmrc ]; then
    echo ".npmrc is present but package.json is missing, the release will not work";
    exit 1;
fi;

# use preinstalled semantic release if missing
if [ ! -f package.json ]; then
    echo "package.json is not present, will use preinstalled semantic release and plugins";
    cp -r /app/node_modules ./;
    if [ -f .npmrc ]; then
        rm .npmrc;
    fi;
else
    echo "package.json is present, will use semantic release and plugins from this package";
    npm ci;
fi;

# workaround for incompatible .npmrc and publishConfig
if [ -f .npmrc ]; then
    sed -i '/@perun:registry=https:\/\/gitlab\.ics\.muni\.cz\/api\/v4\/packages\/npm\//d' .npmrc;
    if [ ! -s .npmrc ]; then
        rm .npmrc;
    fi;
fi;

npx semantic-release
