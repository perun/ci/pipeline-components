# use preinstalled commitlint if missing
if [ ! -f package.json ]; then
    echo "package.json is not present, will use preinstalled commitlint and config";
    cp -r /app/node_modules ./;
    if [ -f .npmrc ]; then
        rm .npmrc;
    fi;
else
    echo "package.json is present, will use commitlint and config from this package";
    npm ci;
fi;

npx commitlint -f "$CI_MERGE_REQUEST_DIFF_BASE_SHA" -t HEAD -V
